const Query = {
  users(parent, args, { db }, info) {
    if (!args.query) {
      return db.users;
    }

    return db.users.filter((user) => {
      return user.name
        .toLocaleLowerCase()
        .includes(args.query.toLocaleLowerCase());
    });
  },
  posts(parent, args, { db }, info) {
    if (!args.query) {
      return db.posts;
    }

    return db.posts.filter((post) => {
      const queryString = post.title + post.body;
      return queryString
        .toLocaleLowerCase()
        .includes(args.query.toLocaleLowerCase());
    });
  },
  comments(parent, args, { db }, info) {
    return db.comments;
  },
  me() {
    return {
      id: 'abc123',
      name: 'Dan',
      email: 'me@me.com',
      age: 54
    };
  },
  post() {
    return {
      id: '12323242',
      title: 'How to Do Something',
      body:
        'A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. A whole bunch of text about how to do something. ',
      published: true
    };
  }
};

export { Query as default };
