import 'cross-fetch/polyfill'
import { gql } from 'apollo-boost'
import seedDatabase, { userOne, postOne, postTwo } from './utils/seedDatabase'
import getClient from './utils/getClient'
import prisma from '../src/prisma';
import { getUsers, getMyPosts, createPost, updatePost, deletePost } from './utils/operations'

const client = getClient()

beforeEach(seedDatabase)

test('Should expose public author profiles', async () => {
  const response = await client.query({ query: getUsers })

  expect(response.data.users.length).toBe(2)
  expect(response.data.users[0].email).toBe(null)
  expect(response.data.users[0].name).toBe('test')

})

test('Should retrieve myPosts', async () => {
  const client = getClient(userOne.jwt)
  const { data } = await client.query({ query: getMyPosts })

  expect(data.myPosts.length).toBe(2)
})

test('Should be able to update own post', async () => {
  const client = getClient(userOne.jwt)
  const variables = {
    id: postOne.post.id,
    data: {
      published: false
    }
  }
  const { data } = await client.mutate({ mutation: updatePost, variables })
  const exists = await prisma.exists.Post({ id: postOne.post.id, published: false })
  
  expect(data.updatePost.published).toBe(false)
  expect(exists).toBe(true)
})

test('Should be able to create a post with authenticated user', async () => {
  const client = getClient(userOne.jwt)
  const variables = {
    data: {
      title: 'Test post 3',
      body: '...',
      published: true
    }
  }

  const { data } = await client.mutate({ mutation: createPost, variables })
  const exists = await prisma.exists.Post({ 
    id: data.createPost.id,
    title: data.createPost.title, 
    body: data.createPost.body,
    published: data.createPost.published
  })

  expect(exists).toBe(true)
})

test('Should be able to delete own post', async () => {
  const client = getClient(userOne.jwt)
  const variables = {
    id: postTwo.post.id
  }
  await client.mutate({ mutation: deletePost, variables })
  const exists = await prisma.exists.Post({ id: postTwo.post.id })
  expect(exists).toBe(false)
})
