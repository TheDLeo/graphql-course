import ApolloClient, { gql } from 'apollo-boost'

const client = new ApolloClient({
  uri: 'http://localhost:4000'
})

const getPosts = gql`
  query {
    posts {
      title
      author {
        name
      }
    }
  }
`

client.query({
  query: getPosts
}).then((response) => {
  let html = ''

  response.data.posts.forEach((post) => {
    html += `
      <div>
        <h3>${post.title}</h3>
        <h4>${post.author.name}</h4>
      </div>
    `
  })

  document.getElementById('posts').innerHTML = html
})